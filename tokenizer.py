import re
from stack import Stack


class NonMatchingNumberOfBraces(Exception):
    pass


class NonMatchingNumberOfOperators(Exception):
    pass


def are_operators_valid(tokens):
    number_of_operators = 0
    number_of_words = 0
    number_of_phrases = 0

    for token in tokens:
        if is_operator(token):
            number_of_operators += 1
        elif not is_brace(token) and '"' not in token:
            number_of_words += 1
    for token in tokens:
        if '"' in token:
            number_of_phrases += 1
    number_of_phrases = number_of_phrases / 2

    if (number_of_words + number_of_phrases - number_of_operators) == 1:
        return True
    return False


def tokenize(expression):
    tokens = expression.split(" ")

    if not are_braces_valid(tokens):
        raise NonMatchingNumberOfBraces

    if is_operator(tokens[0]) or is_operator(tokens[-1]):
        raise NonMatchingNumberOfOperators
    return tokens


def are_braces_valid(tokens):
    left_braces_number = 0
    right_braces_number = 0
    for token in tokens:
        if token == '(':
            left_braces_number += 1
        elif token == ')':
            right_braces_number += 1
    if left_braces_number == right_braces_number:
        return True
    return False


def to_postfix(expression):
    tokens = tokenize(expression)
    s = Stack()
    postfix_expression = []

    for i in range(0, len(tokens)):
        if is_operator(tokens[i]) or tokens[i] == '(':
            s.push(tokens[i])
        elif tokens[i] == ')':
            while s.top() != '(':
                postfix_expression.append(s.pop())
            s.pop()
        else:
            postfix_expression.append(tokens[i])
            try:
                if not is_operator(tokens[i + 1]) and tokens[i + 1] != ')' and '"' not in tokens[i]:
                    s.push("OR")
            except:
                pass

    while not s.is_empty():
        postfix_expression.append(s.pop())

    if not are_operators_valid(postfix_expression):
        raise NonMatchingNumberOfOperators

    return postfix_expression


def is_operator(token):
    if token == 'AND' or token == 'OR' or token == 'NOT':
        return True
    return False


def is_brace(token):
    if token == '(' or token == ')':
        return True
    return False


