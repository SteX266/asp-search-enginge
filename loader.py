import os

from graph import Graph
import html_parser
from trie import Trie


class Loader:

    def __init__(self, root):
        self.graph = Graph(True)
        self.trie = Trie()
        self.parser = html_parser.Parser()
        self.root = root

    def _load(self, root):
        for file_path in os.listdir(root):
            file_path = os.path.join(root, file_path)
            if os.path.isfile(file_path):
                if file_path.endswith(".html"):
                    self.process_html_file(file_path)
            else:
                self._load(file_path)

    def load(self):
        self._load(self.root)
        return self.trie, self.graph

    def process_html_file(self, file_path):
        links, words = self.parser.parse(file_path)
        self._create_graph(file_path, links)
        self.trie.add_page(words, file_path)

    def _create_graph(self, file_path, links):
        main_node = self.get_node(file_path)
        for link in links:
            other_node = self.get_node(link)
            self.graph.insert_edge(main_node, other_node)

    def get_node(self, file_path):
        for node in self.graph.vertices():
            if node.element() == file_path:
                return node
        return self.graph.insert_vertex(file_path)
