from html_parser import Parser
from loader import Loader
from stack import Stack
from tokenizer import to_postfix, NonMatchingNumberOfBraces, NonMatchingNumberOfOperators
from termcolor import colored
from operator import itemgetter


def is_operator(token):
    if token == 'AND' or token == 'OR' or token == 'NOT':
        return True
    return False


def is_and_operator(token):
    if token == 'AND':
        return True
    return False


def is_or_operator(token):
    if token == 'OR':
        return True
    return False


def and_operation(pages1, pages2):
    result_pages = {}
    for page in pages1:
        if page in pages2.keys():
            result_pages[page] = {'value': pages1[page]['value'] + pages2[page]['value'],
                                  'number of words': pages1[page]['number of words'] + pages2[page]['number of words']}
    return result_pages


def or_operation(pages1, pages2):
    for page in pages2:
        if page in pages1.keys():
            pages1[page]['value'] += pages2[page]['value'] + 20
        else:
            pages1[page] = {'value': pages2[page]['value'],
                            'number of words': pages2[page]['number of words']}
    return pages1


def not_operation(pages1, pages2):
    result_pages = {}
    for page in pages2:
        if not page in pages1.keys():
            result_pages[page] = {'value': pages2[page]['value'],
                                  'number of words': pages2[page]['number of words']}
    return result_pages


def should_show_more():
    should_show_more = input("Enter 'm' to show more results\n>>>")
    if should_show_more == 'm':
        return True
    return False


def remove_operators_from_expression(postfix_expression):
    for expression in postfix_expression:
        if is_operator(expression):
            postfix_expression.remove(expression)
    return postfix_expression


class SearchEngine:

    def __init__(self, root):
        self.parser = Parser()
        self.loader = Loader(root)
        self.trie, self.graph = self.loader.load()

    def search(self, inserted_text):
        if len(inserted_text) == 0:
            print('You must enter a phrase, try again!')
            return

        if "*" in inserted_text:
            inserted_text = inserted_text.replace('*', '')
            inserted_text = self.autocomplete(inserted_text)
            if inserted_text == '':
                print('There are no autocomplete results for inserted text, try again.')
                return

        try:
            postfix_expression = to_postfix(inserted_text)
        except NonMatchingNumberOfBraces:
            print('Number of left and right braces must be the same, please try again.')
            return
        except NonMatchingNumberOfOperators:
            print('Operators are used incorrectly, please try again.')
            return
        result = self.postfix_search(postfix_expression)
        print("Total amount of results:{0}\n".format(len(result)))
        if len(result) == 0 and len(postfix_expression) == 1:
            word = self.did_you_mean(inserted_text)
            if word is None:
                return
            while True:
                choice = input('\nDid you mean: {0} ? (yes/no)\n>>>'.format(word))
                if choice != 'yes' and choice !='no':
                    print('Wrong input. Try again.')
                    continue
                elif choice == 'yes':
                    return self.search(word)
                else:
                    break
        new_results = []
        for page in result:
            new_results.append({'key': page,
                                'value': result[page]['value']})
        new_results = sorted(new_results, key=itemgetter('value'), reverse=True)

        postfix_expression = remove_operators_from_expression(postfix_expression)
        postfix_expression = remove_operators_from_expression(postfix_expression)
        postfix_expression = remove_operators_from_expression(postfix_expression)
        postfix_expression = remove_operators_from_expression(postfix_expression)

        self.print_pages(new_results, postfix_expression)

    def postfix_search(self, postfix_expression):
        s = Stack()
        have_a_phrase = False
        phrase_tokens = []
        for token in postfix_expression:
            if not have_a_phrase:
                if is_operator(token):
                    if is_and_operator(token):
                        s.push(and_operation(s.pop(), s.pop()))
                    elif is_or_operator(token):
                        s.push(or_operation(s.pop(), s.pop()))
                    else:
                        s.push(not_operation(s.pop(), s.pop()))
                elif '"' in token:
                    have_a_phrase = True
                    phrase_tokens.append(token)
                    continue
                else:
                    s.push(self.search_word(token))
            else:
                phrase_tokens.append(token)
                if '"' in token:
                    have_a_phrase = False
                    s.push(self.search_phrase(phrase_tokens))
                    phrase_tokens = []
        return s.pop()

    def search_word(self, word):
        pages = self.trie.find_word(word)
        pages = self.valuate_pages(pages)
        return pages

    def get_vertex(self, page):
        for vertex in self.graph.vertices():
            if vertex.element() == page:
                return vertex

    def valuate_pages(self, pages):
        for page in pages.values():
            page['value'] = page['number of words'] * 3

        for page in pages:
            vertex = self.get_vertex(page)
            vertex_edges = self.graph.incident_edges(vertex, False)
            for edge in vertex_edges:
                pages[page]['value'] += 2
                if edge.opposite(vertex).element() in pages.keys():
                    pages[page]['value'] += pages[edge.opposite(vertex).element()]['number of words'] / 3
        return pages

    def print_pages(self, pages, words):
        print("Number of results: {0}".format(len(pages)))
        number_of_printed_results = 0
        i = 0
        for page in pages:
            number_of_printed_results += 1
            print(str(i * 10 + number_of_printed_results) + '.\n')
            self.print_page(page, words)
            if number_of_printed_results == 10:
                number_of_printed_results = 0
                i += 1
                if not should_show_more():
                    break

    def print_page(self, page, old_words):
        title = page['key'].split('\\')[-1]
        print(title + '\n')

        text = ''
        words = []
        for word in old_words:
            word = word.lower()
            if '"' in word:
                word = word.replace('"', '')
            words.append(word)

        l, all_words = self.parser.parse(page['key'])
        i = 0
        for word in all_words:
            if word.lower() in words:
                if i > 10:
                    for j in range(i - 10, i + 15):
                        if all_words[j].lower() in words:
                            text += colored(all_words[j], 'red')
                        else:
                            text += all_words[j]
                        text += ' '
                else:
                    for j in range(0, i + 20):
                        if all_words[j].lower() in words:
                            text += colored(all_words[j], 'red')
                        else:
                            text += all_words[j]
                        text += ' '
                break
            i += 1

        print(text)

    def search_phrase(self, words):

        new_pages = {}
        words[0] = words[0].replace('"', '')
        words[-1] = words[-1].replace('"', '')
        pages1 = self.trie.find_word(words[0])
        pages1 = self.valuate_pages(pages1)
        for i in range(1, len(words)):
            pages2 = self.trie.find_word(words[i])
            pages2 = self.valuate_pages(pages2)
            pages1 = and_operation(pages1, pages2)

        for page in pages1:
            is_phrase = False
            l, all_words = self.parser.parse(page)
            word_index = 0
            for word in all_words:
                if word == words[0]:
                    for i in range(0, len(words) - 1):
                        if words[i] == all_words[i + word_index]:
                            is_phrase = True
                        else:
                            is_phrase = False
                            break
                word_index += 1
            if is_phrase:
                new_pages[page] = {'value': pages1[page]['value'],
                                   'number of words': pages1[page]['number of words']}
        return new_pages

    def autocomplete(self, text):
        words = self.trie.starts_with(text)
        best_words = self.find_most_used_words(words)
        if len(best_words) != 1 or text not in best_words:
            while True:
                word_index = 0
                for word in best_words:
                    print ('\n{0}. {1}'.format(str(word_index+1),word['key']))
                    word_index += 1
                    if word_index == 5:
                        break

                number = input('\nInsert number of a word you want to search for\n>>>')

                if number.isdigit():
                    if int(number) > word_index or int(number) < 0:
                        continue
                    break
                print("You must insert a digit")
            return best_words[int(number) - 1]['key']
        else:
            return ''

    def find_most_used_words(self, words):
        best_words = []
        for word in words:
            possible_word = self.trie.find_word(word)
            best_words.append({'key': word,
                               'number of words': len(possible_word)})

        best_words = sorted(best_words, key=itemgetter('number of words'), reverse=True)
        return best_words

    def did_you_mean(self, text):
        possible_beginnings = self.create_possible_word_beginnings(text)
        for i in range (0,len(possible_beginnings) - 1):
            possible_word = self.trie.starts_with(possible_beginnings[i])
            if len(possible_word) < 3:
                possible_words = self.trie.starts_with(possible_beginnings[i-1])
                return self.find_most_used_words(possible_words)[0]['key']

    def create_possible_word_beginnings(self, text):
        possible_beginnings = []
        possible_beginnings.append(text[0])
        for i in range (1,len(text) - 1):
            possible_beginnings.append(possible_beginnings[i-1] + text[i])
        return possible_beginnings