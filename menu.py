from search_engine import SearchEngine


def menu():
    search_engine = SearchEngine(
        "C:\\Users\\SteX\\Desktop\\ASP Search Engine\\asp-search-enginge\\python-3.8.3-docs-html")

    user_search(search_engine)


def should_exit():
    should_continue = input('Press anything to continue searching or "x" for exit\n>>>')
    if should_continue == 'x' or should_continue == 'X':
        return True
    return False


def user_search(search_engine):
    while True:
        user_text = input('Insert the phrase you want to search for:\n>>>')
        search_engine.search(user_text)
        if should_exit():
            break



if __name__ == '__main__':
    menu()
