class TrieNode(object):
    """
    Our trie node implementation. Very basic. but does the job
    """

    def __init__(self, char):
        self.char = char
        self.children = []
        self.pages = {}


class Trie(object):

    def __init__(self):
        self._root = TrieNode("*")

    def add(self, word, page):
        """
        Adding a word in the trie structure
        """
        node = self._root
        word = word.lower()
        for char in word:
            found_in_child = False
            # Search for the character in the children of the present `node`
            for child in node.children:
                if child.char == char:
                    node = child
                    found_in_child = True
                    break
            # We did not find it so add a new chlid
            if not found_in_child:
                new_node = TrieNode(char)
                node.children.append(new_node)
                # And then point node to the new child
                node = new_node
        # Everything finished. Mark it as the end of a word.
        if page in node.pages.keys():
            node.pages[page]['number of words'] += 1
        else:
            node.pages[page] = {'number of words':1,
                                'value':0}

    def add_page(self, words, page):
        """
        Adds all words from a page to trie
        """
        for word in words:
            self.add(word, page)

    def find_word(self, word):
        """
        Check and return
          1. If the word exists in the trie
          2. If yes then how many times has it been added
        """
        node = self._root
        word = word.lower()
        # If the root node has no children, then return False.
        # Because it means we are trying to search in an empty trie
        if not self._root.children:
            return {}
        for char in word:
            char_not_found = True
            # Search through all the children of the present `node`
            for child in node.children:
                if child.char == char:
                    # We found the char existing in the child.
                    char_not_found = False
                    # Assign node as the child containing the char and break
                    node = child
                    break
            # Return False anyway when we did not find a char.
            if char_not_found:
                return {}

        return node.pages

    def starts_with(self, start):
        node = self._root
        word = start.lower()
        if not self._root.children:
            return []
        for char in word:
            for child in node.children:
                if child.char == char:
                    node = child
                    break
        return self._ends_with(node, start)

    def _ends_with(self, node, start):
        words_list = []
        if not node.children:
            return start
        if node.pages != {}:
            words_list.append(start)
        for child in node.children:
            word = self._ends_with(child, start + child.char)
            if isinstance(word, list):
                words_list += word
            else:
                words_list.append(word)
        return words_list
